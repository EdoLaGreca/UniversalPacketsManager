Universal Packets Manager
=========================
Info
----

### State
##### Windows
![Version](https://img.shields.io/badge/version-alpha%201.0.0-green.svg)
![Build status](https://img.shields.io/badge/build-no%20build-red.svg)

##### macOS
![Version](https://img.shields.io/badge/version-alpha%201.0.0-green.svg)
![Build status](https://img.shields.io/badge/build-passed-brightgreen.svg)

##### Linux
![Version](https://img.shields.io/badge/version-alpha%201.0.0-green.svg)
![Build status](https://img.shields.io/badge/build-passed-brightgreen.svg)

---

### What is the Universal Packets Manager?
The *Universal Packets Manager* is a simple program written with [**Qt**](https://www.qt.io) that let you work with *uPlatform*: a space where *you* can publish your software and distribute it.  

### What is your goal?
My goal is to create a completely new platform to make *easy* the software's download process and to **limit viruses** and **web piracy**.

---

### Prices
There are several offers that I can suggest to you:  
##### Basic
+ **250MiB** of total storage.
+ Up to **5** repositories.

Price: free.

##### Premium
+ **500MiB** of total storage.
+ Up to **15** repositories.

Price: (not yet decided)

##### VIP
+ **5GiB** of total storage.
+ Up to **30** repositories.

Price: (not yet decided)

##### VIP+
+ **10GiB** of total storage.
+ Up to **100** repositories.

Price: (not yet decided)

*NOTE*: the "total storage" is **not** the limit for **each** repository, but the limit for the **sum** of **all** the repositories.