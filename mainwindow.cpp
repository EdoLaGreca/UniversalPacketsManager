#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDialog>
#include <QInputDialog>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Create the table model
    QStandardItemModel *model = new QStandardItemModel(0,4,this); // edit also this if you want to add rows/columns (<rows>, <columns>, this)
    model->setHorizontalHeaderItem(0, new QStandardItem(QString("Name")));
    model->setHorizontalHeaderItem(1, new QStandardItem(QString("Developer")));
    model->setHorizontalHeaderItem(2, new QStandardItem(QString("Size")));
    model->setHorizontalHeaderItem(3, new QStandardItem(QString("Description")));
     // add columns/rows here...
    ui->uPackets->setModel(model);
    ui->uPackets->resizeColumnsToContents();

	statusBar()->showMessage(tr("Ready."));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actSearch_uPacket_triggered()
{
	bool ok;
	QString query = QInputDialog::getText(this, tr("Input"), tr("Type a query:"), QLineEdit::Normal, "", &ok);
	if (query.length() != 0 && ok) {
		statusBar()->showMessage(tr("Connecting...."));
		// connect to database through SSH
		statusBar()->showMessage(tr("Searching...."));
		bool found = false;
		// search inside database (name and description)
		if (found) {
			// put results on the table
		} else {
			// message box: no uPackets found with this search query
		}


	}

}

void MainWindow::on_actAbout_triggered()
{
	QMessageBox about;
	about.addButton(QMessageBox::Close);
	about.addButton(QMessageBox::Ok);
	about.setWindowTitle("About...");
	about.setText("Info:");
	about.setInformativeText("Version: 1.0.0<br>Made with Qt."); // change application version here --------------------------------------------------------------------------
	about.exec();
	about.setFixedWidth(500);
}
