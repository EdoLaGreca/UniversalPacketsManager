#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStatusBar>
#include <QTableView>
#include <QStandardItemModel>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
	void on_actSearch_uPacket_triggered();

	void on_actAbout_triggered();

private:
    Ui::MainWindow *ui;
	QTableView *uPackets;
	QToolBar *toolBar;

};

#endif // MAINWINDOW_H
